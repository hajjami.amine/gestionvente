import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ResourceBundle;
import java.io.*;

public class InteractionFiles {
	
	private static ResourceBundle rb = ResourceBundle.getBundle("paramConf");
	
	public static void main(String[] args) {
		
		//Exercice 1
		String file_lecture     = rb.getString("file_lecture");
		System.out.println(file_lecture);
		String file_ecriture     = rb.getString("file_ecriture");
		System.out.println(file_ecriture);
		
		
		try {
			// Exercice 2 Reader
			FileReader fr=new FileReader("C:\\Users\\Y50\\Desktop\\Formation\\workspace\\GestionVente\\src\\paramConf.properties");
			BufferedReader br= new BufferedReader(fr); 
			while (br.ready()) 
				System.out.println(br.readLine()); 
			br.close(); 
			
			//Exercice 3 Writer
			FileWriter fw=new FileWriter("C:\\Users\\Y50\\Desktop\\Formation\\workspace\\GestionVente\\src\\paramConf.properties", true);
			BufferedWriter bw= new BufferedWriter(fw); 
			bw.newLine();
			bw.write("Test");
			bw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
