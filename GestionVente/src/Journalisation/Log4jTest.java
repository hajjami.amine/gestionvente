package Journalisation;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager; 
import org.apache.log4j.Logger;

public class Log4jTest{

  static Logger log = LogManager.getLogger(Log4jTest.class.getName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//BasicConfigurator.configure();
	      log.trace("Trace Message!");
	      log.debug("Debug Message!");
	      log.info("Info Message!");
	      log.warn("Warn Message!");
	      log.error("Error Message!");
	      log.fatal("Fatal Message!");

	}

}
