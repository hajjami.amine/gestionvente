package ThreadsetConcurrence;


public class TestCompte implements Runnable {
	
	private Compte cpt;
	private String threadName;
	
	public TestCompte(Compte cpt, String threadName) {
		super();
		this.cpt = cpt;
		this.threadName = threadName;
	}

	public void run() {
		synchronized (cpt) {
			for (int i = 0; i < 10; i++) {
				if(cpt.getSolde()>0){
					System.out.println("solde en compte "+cpt.getSolde()+" "+threadName);
					cpt.retirer(10);
					System.out.println(threadName+" retrait fait reste "+cpt.getSolde());
				}
			}
		}
		
		
	}
	
}
