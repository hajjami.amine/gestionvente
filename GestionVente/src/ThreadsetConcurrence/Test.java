package ThreadsetConcurrence;

public class Test {

	public static void main(String args[]){
		Compte cpt =  new Compte();
		
		Thread t1 =  new Thread(new TestCompte(cpt,"t1"));
		Thread t2 =  new Thread(new TestCompte(cpt,"t2"));
		
		t1.start();
		t2.start();
	}


}
