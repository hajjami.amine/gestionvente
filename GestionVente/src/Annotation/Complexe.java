package Annotation;

import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Instable {}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Auteur{
	String value();
}
class Complexe {
   private double r�el;
   private double imaginaire;
   
   public Complexe(double r, double i) {
      r�el = r;
      imaginaire = i;
   }

   @Override public String toString() {
      return "["+r�el+", "+imaginaire+"]";
   }
   @Auteur("AMINE")
   @Instable public double module() { return 0.0; }
   @Instable public double argument() { return 0.0; }
}

