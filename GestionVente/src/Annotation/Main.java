package Annotation;

import java.lang.reflect.Method;

public class Main {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException {
		  Class classe = Complexe.class;
	      Method methode = classe.getMethod("module");
	      boolean instable = methode.isAnnotationPresent(Instable.class);
	      String auteur = methode.getAnnotation(Auteur.class).value();
	      System.out.println("auteur annotation : "+auteur);
	      System.out.println(instable);
	}

}
