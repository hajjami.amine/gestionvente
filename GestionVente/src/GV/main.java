package GV;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;



public class main {
	public static ArrayList<Article> importArticlesCSV(String path){
		///Charger les articles a partir d'un fichier csv
				FileReader fr;
				ArrayList<Article> ListeArticles = new ArrayList<Article>();
				try {
					fr = new FileReader(path);
					BufferedReader br= new BufferedReader(fr); 
					int i=0;
					while (br.ready()){
						if(i==0){br.readLine();i++;continue;}
					        String[] values = br.readLine().split(",");
					        Article articleCharg = new Article();
					        articleCharg.setTitre(values[0]);
					        articleCharg.setPrix(Double.valueOf(values[1]));
					        articleCharg.setQuantit�(Integer.valueOf(values[2]));
					        articleCharg.setPromo(Promotion.valueOf(values[3]));
					        System.out.println(articleCharg.toString());
					        int nombreProduit = values.length - 4;
							
							for (int j=0; j< nombreProduit; j++){
								articleCharg.stocker(articleCharg.ajouter(values[4+j]));	
							}
					        ListeArticles.add(articleCharg);
					}
					br.close();
				} catch (IOException e) {
						e.printStackTrace();
				}
				return ListeArticles;
	}
	public static void exporterArticlesCSV(Article article,String path){
		FileWriter fw;
		try {
			fw = new FileWriter(path,true);
			BufferedWriter bw= new BufferedWriter(fw); 
			bw.write("titre"+","+"prix"+","+"quantit�"+","+"promotion"+","+"produits");
			bw.newLine();
			bw.write(article.getTitre()+","+article.getPrix()+","+article.getQuantit�()+","+article.getPromo()+",");
			//Selectionner la liste des produits
			for(int i = 0;i<article.getListProduit().size();i++){
				bw.write(article.getListProduit().get(i).getTitre()+',');
			}
			bw.newLine();
			bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Article article = new Article("test", 100, 1, Promotion.ETE);
		article.stocker(article.ajouter("produit"));
		article.stocker(article.ajouter("produit2"));
		//Afficher Article
		System.out.println("Afficher un article"+article);
		//Afficher les produits
		System.out.println("Afficher les produits"+article.getListProduit());
	
		//le chemin du fichier CSV
		String path = "C:\\Users\\Y50\\git\\gestionvente\\GestionVente\\src\\Articles.csv";
		//Exporter l'article
		exporterArticlesCSV(article,path);
		
		ArrayList<Article> liste=importArticlesCSV(path);
		System.out.println(liste.toString());
	
		/*
		TelRecord client = new TelRecord("amine", "hajjami", "0256341");
		TelRecord client1 = new TelRecord("amine1", "hajjami1", "02563411");
		/*Liste des records init*///ArrayList<TelRecord> listeTel = new ArrayList<TelRecord>();
		/*ajouter record*/   //listeTel.add(client);
		//listeTel.add(client1);
		/*supprimer record*/ //listeTel.remove(client);
		/*Lister record */   //System.out.println(listeTel);
		
		/*
		ThreadTelrecord t1=  new ThreadTelrecord(client, listeTel, "add ArrayList","t1");
		ThreadTelrecord t2=  new ThreadTelrecord(client1, listeTel, "add csv","t2");
		System.out.println("Avant"+t1.getListTel().toString());
		t1.start();
		t2.start();
		System.out.println("Apres"+listeTel.toString());
		*/
	}

}
