package GV;

public enum Promotion {
	ETE("ETE"),
	PRINTEMPS("PRINTEMPS"),
	AUTOMNE("AUTOMNE"),
	HIVER("HIVER"),
	NOMAL("");
	
	private String abv;
	
	private Promotion(String abv){
		this.abv = abv;
	
	}
	// Calcule de prix selon la promotion Saison
	double calcPrix(double prix){
		switch(this){
		case ETE : return prix * 0.9;
		case PRINTEMPS : return prix * 0.7;
		case AUTOMNE : return prix * 0.95;
		case HIVER : return prix * 0.8;
		case NOMAL : return prix * 1.0;
		default : 
			return 0.;		}
		
		
	}
	public String getAbv() {
		return abv;
	}

	public void setAbv(String abv) {
		this.abv = abv;
	}
	
}
