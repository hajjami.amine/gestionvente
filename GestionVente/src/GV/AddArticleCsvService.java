package GV;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class AddArticleCsvService {

public  void exporterArticlesCSV(Article article,String path){
		FileWriter fw;
		try {
			fw = new FileWriter(path,true);
			BufferedWriter bw= new BufferedWriter(fw); 
			bw.write("titre"+","+"prix"+","+"quantit�"+","+"promotion"+","+"produits");
			bw.newLine();
			bw.write(article.getTitre()+","+article.getPrix()+","+article.getQuantit�()+","+article.getPromo()+",");
			//Selectionner la liste des produits
			for(int i = 0;i<article.getListProduit().size();i++){
				bw.write(article.getListProduit().get(i).getTitre()+',');
			}
			bw.newLine();
			bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
}
