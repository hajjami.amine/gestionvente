package GV;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class TelRecord {
	private String nom;
	private String prenom;
	private String numTel;

	
	public TelRecord() {	
		super();

	}

	public TelRecord(String nom, String prenom, String numTel) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numTel = numTel;
	}
	
	@Override
	public String toString() {
		return "TelRecord [nom=" + nom + ", prenom=" + prenom + ", numTel="
				+ numTel + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	
	//Exporter Record
	public void exporter(String path){
		FileWriter fw;
		try {
			File directory = new File(path);
			fw = new FileWriter(directory,true);
			BufferedWriter bw= new BufferedWriter(fw);
			bw.write("nom"+","+"prenom"+","+"numTEl");
			bw.newLine();
			bw.write(this.getNom()+","+this.getPrenom()+","+this.getNumTel());
			bw.newLine();
			bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}


}
