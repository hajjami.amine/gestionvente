package GV;
import java.util.ArrayList;


public class ThreadTelrecord extends Thread{
	
	private TelRecord telrec;
	private String ThName;
	private ArrayList<TelRecord> listTel;
	private String operation;
	



	public ThreadTelrecord(TelRecord telrec, ArrayList<TelRecord> listTel,
			String operation,String ThName) {
		super();
		this.telrec = telrec;
		this.listTel = listTel;
		this.operation = operation;
		this.ThName = ThName;
	}
	
	public String getThName() {
		return ThName;
	}

	public void setThName(String thName) {
		ThName = thName;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public TelRecord getTelrec() {
		return telrec;
	}

	public void setTelrec(TelRecord telrec) {
		this.telrec = telrec;
	}

	public ArrayList<TelRecord> getListTel() {
		return listTel;
	}

	public void setListTel(ArrayList<TelRecord> listTel) {
		this.listTel = listTel;
	}

	public void run(){

			if(this.getOperation().equals("add ArrayList")){
				System.out.println(this.getThName());
				listTel.add(telrec);
			}
			else if (this.getOperation().equals("add csv")) {
				System.out.println(this.getThName());
				telrec.exporter("C:\\Users\\Y50\\git\\gestionvente\\GestionVente\\src\\TelRec.csv");
				
			}
			
	}
}
