package GV;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;




@SuppressWarnings("serial")
public class Article implements Serializable{
	private String titre;
	private double prix;
	private int quantit�;
	private Promotion promo;
	private ArrayList<Produit> ListProduit;
	
	
	public Article() {
		super();
		ListProduit = new ArrayList<Article.Produit>();
	}

	public Article(String titre, double prix, int quantit�,Promotion promo) {
		super();
		this.titre = titre;
		this.prix = prix;
		this.quantit� = quantit�;
		this.promo = promo;
	
		ListProduit = new ArrayList<Article.Produit>();
	}
    // Stocker un produit
	public void stocker(Produit produit){
		ListProduit.add(produit);
	}
	//creation produit
	public Produit ajouter(String titre){
		Produit produit = new Produit(titre);
		return produit;
	}
	
	public Promotion getPromo() {
		return promo;
	}

	public void setPromo(Promotion promo) {
		this.promo = promo;
	}

	//Get product
	public ArrayList<Produit> getProduit(){
		return ListProduit;
	}
	
	//find product
	public Produit findProduct(String titre){
		for (int i = 0; i < ListProduit.size(); i++) {
			if(ListProduit.get(i).titre.equals(titre)){
				System.out.println(ListProduit.get(i).toString());
				return ListProduit.get(i);
			}
				
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "Article [titre=" + titre + ", prix=" + prix + ", quantit�="
				+ quantit� + ", promo=" + promo + ", ListProduit="
				+ ListProduit + "]";
	}

	public ArrayList<Produit> getListProduit() {
		return ListProduit;
	}
	
	public void setListProduit(ArrayList<Produit> listProduit) {
		ListProduit = listProduit;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public double getPrix() {
		prix = this.promo.calcPrix(prix);
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getQuantit�() {
		return quantit�;
	}

	public void setQuantit�(int quantit�) {
		this.quantit� = quantit�;
	}
	//Exporter Article To csv
	public void exporter(String path){
			FileWriter fw;
			try {
				fw = new FileWriter(path,true);
				BufferedWriter bw= new BufferedWriter(fw); 
				bw.write("titre"+","+"prix"+","+"quantit�"+","+"promotion"+","+"produits");
				bw.newLine();
				bw.write(this.getTitre()+","+this.getPrix()+","+this.getQuantit�()+",");
				//Selectionner la liste des produits
				for(int i = 0;i<this.ListProduit.size();i++){
					bw.write(ListProduit.get(i).titre+',');
				}
				bw.newLine();
				bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
	//Class interne Produit
	@SuppressWarnings("serial")
	
	class Produit implements Serializable{
		

		private String titre;
		
		public Produit() {
			super();
		}
		public String getTitre() {
			return titre;
		}


		public void setTitre(String titre) {
			this.titre = titre;
		}

		public Produit(String titre) {
			super();
			this.titre = titre;
		}
		@Override
		public String toString() {
			return "Produit [titre=" + titre + "]";
		}
		
		
		
	}
	
}
