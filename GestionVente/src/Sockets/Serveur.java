package Sockets;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.*;

public class Serveur {

	public static void main(String[] zero){
			
			ServerSocket socket;
			try {
			socket = new ServerSocket(3000);
			Thread t = new Thread(new Accepter_clients(socket));
			t.start();
			System.out.println("Serveur pr�t !");
			
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
}

class Accepter_clients implements Runnable {

	   private ServerSocket socketserver;
	   private Socket socket;
	   private int nbrclient = 1;
	   private ObjectInputStream inStream = null;
		public Accepter_clients(ServerSocket s){
			socketserver = s;
		}
		
		public void run() {

	        try {
	        	while(true){
	        		socket = socketserver.accept(); // Un client se connecte on l'accepte
	        		System.out.println("Connected");
	        		InputStream is = socket.getInputStream();
	        		InputStreamReader isr = new InputStreamReader(is);
	                BufferedReader br = new BufferedReader(isr);
	                String produit = br.readLine();
	                System.out.println("Message received from client is "+produit);
	                 socket.close();
	        	}
	        
	        } catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

