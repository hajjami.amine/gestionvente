package Sockets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import GV.Article;
import GV.Promotion;


public class Client {
	
	public static void main(String[] zero) {
		
		Socket socket;
		ObjectOutputStream outputStream = null;
		try {
			
		     socket = new Socket(InetAddress.getLocalHost(),3000);	
			 
			 OutputStream os = socket.getOutputStream();
             OutputStreamWriter osw = new OutputStreamWriter(os);
             BufferedWriter bw = new BufferedWriter(osw);
  
             String produit = "Produit";
  
             String sendMessage = produit + "\n";
             bw.write(sendMessage);
             bw.flush();
             System.out.println("Message sent to the server : "+sendMessage);
  
             

	         socket.close();

		}catch (UnknownHostException e) {
			
			e.printStackTrace();
		}catch (IOException e) {
			
			e.printStackTrace();
		}
	}

}
